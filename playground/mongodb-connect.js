//const MongoClient = require('mongodb').MongoClient;
const {MongoClient,ObjectId} = require('mongodb');
MongoClient.connect('mongodb://localhost:27017/Todo-App',(err,db) => {
  if(err){
    console.log('Unable to connect to MongoDB servers');
  }
  else {
    console.log('Succesfully Connected To The Database Server');
    db.collection('Todos').insertOne({name : 'complete node.js course',duration : '10 days'},(err,res) => {
      if(err){
        console.log('Unable to add item');
      }
      else {
        console.log(JSON.stringify(res.ops,undefined,2));
      }
    });
    db.collection('Users').insertOne({name : 'Sriharsh',age : 19},(err,res) => {
      if(err){
        console.log('Unable to add item');
      }
      else {
        console.log(JSON.stringify(res.ops,undefined,2));
      }
    });
  }
  db.close();
});
