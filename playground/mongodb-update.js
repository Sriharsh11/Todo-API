//const MongoClient = require('mongodb').MongoClient;
//ES6 feature  which lets you extract the ObectId from mongodb
const {MongoClient,ObjectId} = require('mongodb');
MongoClient.connect('mongodb://localhost:27017/Todo-App',(err,db) => {
  if(err){
    console.log('Unable to connect to MongoDB servers');
  }
  else {
    db.collection('Users').findOneAndUpdate({
      _id : new ObjectId('5bad09f80324af322251846e')
    },{
      $set : {
        name : 'Sriharsh Aditya'
      }
    },).then((result) => {
      console.log(result);
    });
    db.collection('Todos').findOneAndUpdate({
      _id : new ObjectId('5bad3803688c898f52a60ffb')
    },{
      $set : {
        completed : true
      }
    }).then((result) => {
      console.log(result);
    });
    db.collection('Users').findOneAndUpdate({
      _id : new ObjectId('5bad09f80324af322251846e')
    },{
      $inc : {
        age : 1
      }
    }).then((result) => {
      console.log(result);
    });
  }
  //db.close();
});
