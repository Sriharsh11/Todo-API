//const MongoClient = require('mongodb').MongoClient;
//ES6 feature  which lets you extract the ObectId from mongodb
const {MongoClient,ObjectId} = require('mongodb');
MongoClient.connect('mongodb://localhost:27017/Todo-App',(err,db) => {
  if(err){
    console.log('Unable to connect to MongoDB servers');
  }
  else {
    db.collection('Users').deleteMany({name : 'Shubham'}).then((result) => {
      console.log(result);
    },(err) => {
      console.log(err);
    });
    db.collection('Users').deleteOne({_id : new ObjectId('5bad191f688c898f52a60823')}).then((result) => {
      console.log(result);
    },(err) => {
      console.log(err);
    });
    db.collection('Users').findOneAndDelete({_id : new ObjectId('5bad194f688c898f52a6082f')}).then((result) => {
      console.log(result);
    },(err) => {
      console.log(err);
    });
  }
  //db.close();
});
