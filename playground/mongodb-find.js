//const MongoClient = require('mongodb').MongoClient;
//ES6 feature  which lets you extract the ObectId from mongodb
const {MongoClient,ObjectId} = require('mongodb');
MongoClient.connect('mongodb://localhost:27017/Todo-App',(err,db) => {
  if(err){
    console.log('Unable to connect to MongoDB servers');
  }
  else {
    db.collection('Users').find({age : 19}).count().then((count) => {
      console.log(`The people with age 19 are : ${count}`);
    },(err) => {
      console.log(err);
    });
  }
  //db.close();
});
